#! /bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################

function preprocess()
{

echo "LPROCESS"

TMP_FILE1="file1.tmp"
TMP_FILE2="file2.tmp"

sed -i '/\%essai\ rt_token/d' $1
sed -i '/\%!ignore_include/d' $1
cp $1 $TMP_FILE1
old_IFS=$IFS
IFS=$'\n'
cat $2 | grep  -e ^@ | sed 's/[[:blank:]]\+/ /g' |  sed 's/[[:blank:]]*$//g'  |  sed  "s/^[[:blank:]]*@//" > NewVar
for ligne in $(cat NewVar)
do

        var1=`echo "$ligne" | cut -d " " -f1`;
        var2=`echo "$ligne" | cut -d " " -f2`;
	#echo "remplace $var1 par $var2"
        sed "s/$var1/$var2/g" $TMP_FILE1 > $TMP_FILE2

        mv $TMP_FILE2 $TMP_FILE1
done
IFS=$old_IFS
mv $TMP_FILE1 $3
rm NewVar
	#on rend l entree plus robuste : suppression des doubles espaces et esoaces de fin de ligne
	#VAR=`echo "$VAR2" | sed 's/[[:blank:]]\+/ /g'`
#	VAR=`echo "$VAR2" | sed 's/[[:blank:]]*$//g'`
#	TMP_FILE="$3.tmp"
	
	#chaine de HASH : on regroupe les lignes en 1 seule ordonnee avec les @
#	VAR2=`echo "$VAR" | xargs`
#	CHAINE_HASH=`echo "$VAR2" | sed 's/[[:blank:]]@/@/g'`
#	echo "Indexed string : $CHAINE_HASH"
	
	#chaine de recherche contenant les noms des varibles
#	SUBSRES=""
#	SUBS=`echo "$VAR" | sed "s/\(@\)\(.*\)[[:blank:]]\(.*\)$/\2/"`
#	SUBSRES=`echo $SUBS | xargs`
#	echo "Symboles : $SUBSRES"
	#comptage des variables
#	NBRE=`echo "$SUBSRES" | wc -w`
#	echo "Number of substitutions : $NBRE"
	
	#rajout des symboles pour _ou_ pour l expression reguliere, suppression des espaces, suppression du | en trop
#	REG_EXP=`echo "$SUBSRES" | sed 's/\([^[:blank:]]\+\)/\1\\\|/g'`
#	REG_EXP1=`echo "$REG_EXP" | sed "s/[[:blank:]]//g"`
#	REG_EXP=`echo "$REG_EXP1" | sed 's/\\\|$//g'`
#	echo "Regular expression : $REG_EXP"
	
	#script sed
#	echo '#! /bin/sed -f' > $TMP_FILE
#	echo '' >> $TMP_FILE
	#ajout du HASH a la fin de ligne
#	echo 's/$/'$CHAINE_HASH'/' >> $TMP_FILE
	#remplacement des variables par leur valeur contenu dans le hash
#	echo ':label;s/\([^@]*\)\('$REG_EXP'\)\(.*\)@\2 \([^@]*\)/\1\4\3@\2 \4/;t label' >> $TMP_FILE
#	echo 's/\([^@]*\).*$/\1/' >> $TMP_FILE

	#lancement du script sed et suppression des fichiers temporaires
#	chmod u+x $TMP_FILE
#	sed -f $TMP_FILE $1 > $3
#	rm $TMP_FILE
}


#Calcul des expression pour les scripts : {1# s(1/2) } avec {precison_calcul# expression comprise par bc}
function calcul()
{
	if [ ! -f $1 ]
	then
		echo "Files $1 does not exist... check the parameters"
		exit 2
	fi
	
	cp $1 $1.bak -f
	
	#Recherche un expression a calculer puis a remplacer
	CHAINE=$( grep -n -m 1 -o "{.*}" $1.bak )
	while [ ! -z "$CHAINE" ]
	do
		#recup de la ligne pour accelerer sed
		MOTIF_TEMP=${CHAINE#*:}
		#recup du reste sans la ligne
		LIGNE=${CHAINE%%:$MOTIF_TEMP}
		#recup d'un seul calcul : au cas ou y'en a plusieurs sur la meme ligne
		MOTIF=`echo "$MOTIF_TEMP" | sed 's/\({[[:digit:]]\+#[^#]*}\).*/\1/'`
		#recup du calcul
		CALCUL=`echo "$MOTIF" | sed 's/{[[:digit:]]\+#\([^#]*\)}.*/\1/'`
		
		#recup de la precision de calcul
		SCALE=`echo "$MOTIF" | sed 's/{\([[:digit:]]\+\)#.*}/\1/'`
	
		#ecriture du script pour bc
		echo "scale = $SCALE" > calc.bc
		echo "print ( $CALCUL )" >> calc.bc
		echo "quit" >> calc.bc
	
		#Calcul par bc
		RES=$(bc -l calc.bc)
		echo "Le motif : $MOTIF ligne $LIGNE a ete evalue a : $RES"
		
		#on remplace les caracteres speciaux de calcul pour former l expression reguliere : multiplication et division
		MOTIF=$(echo "$MOTIF" | sed 's/\//\\\//g')
		MOTIF=$(echo "$MOTIF" | sed 's/\*/\\\*/g')
		#substitution de l'expression par le resultat du calcul
		cat "$1.bak" | sed $LIGNE"s/$MOTIF/$RES/g" > $1
		mv $1 $1.bak
		
		
		#on reitere pour une autre expression
		CHAINE=$( grep -n -m 1 -o "{.*}" $1.bak )
	done
	
	mv $1.bak $1
	rm -f calc.bc
}
	

	#programme principal
	#verif
	if [ ! $# == 3 ]
	then
		echo "This script takes 3 parameters : my_script.symb file_with_symbols.var final.script" 
		echo "Takes care of the parameters order!"
		exit 1
	fi

	if [ ! -f $1  ]
	then
		echo -e "\n ERROR : File $1 does not exist... check the parameters\n\n"
		exit 2
	fi
	if [ ! -f $2 ]
	then
		echo -e "\n ERROR : File $2 does not exist...\n"
		exit 3
	fi

	#on preporcess pour remplacer les variables
	preprocess $1 $2 $3
	#on calcul les expressions vu que les variables ont ete remplacees par leur valeur
	calcul $3 $3
	#Et voili voilou : c'est trop cool!
	echo "File $1 preprocessed with $2 -> $3!!!"


	echo '  ____     _____    _____    __          __     '
	echo ' /\  _`\  /\  __`\ /\  __`\ /\ \        /\ \    '
	echo ' \ \ \/\_\\ \ \/\ \\ \ \/\ \\ \ \       \ \ \   '
	echo '  \ \ \/_/_\ \ \ \ \\ \ \ \ \\ \ \  __   \ \_\  '
	echo '   \ \ \L\ \\ \ \_\ \\ \ \_\ \\ \ \L\ \   \/_/_ '
	echo '    \ \____/ \ \_____\\ \_____\\ \____/     /\_\'
	echo '     \/___/   \/_____/ \/_____/ \/___/      \/_/M.M.'
	
	exit 0


#echo '#! /bin/sed -f' > azerty_tmp
#echo '' >> azerty_tmp
#echo 's/$/@TOTO 1@TITI 2@TATA 3/' >> azerty_tmp
#echo 's/\([^@]*\)\(\(TOTO\)\|\(TITI\)\|\(TATA\)\)\(.*\)@\2 \([^@]*\)/\1\7\6@\2 \7/' >> azerty_tmp
#echo 's/\([^@]*\).*$/\1/' >> azerty_tmp
#
#chmod u+x ./azerty_tmp
#./azerty_tmp $1
#rm ./azerty_tmp

#nbre_1=2
#nbre_2=3
#echo "scale = 0" > calc.bc
#echo "print ( $nbre_1 / $nbre_2 )" >> calc.bc
#echo "quit" >> calc.bc
#
#res=$(bc -l calc.bc)
#
#echo "$res"
#
#rm calc.bc -f

#echo "nbre_1 = read()" > calc.bc
#echo "nbre_2 = read()" >> calc.bc
#echo "print ( nbre_1 / nbre_2 )" >> calc.bc
#echo "quit" >> calc.bc
#
#res=$(bc -l calc.bc <<- fin
#        2
#	        3
#		fin
#		)
#
#		echo "$res"
#
#		rm calc.bc -f
