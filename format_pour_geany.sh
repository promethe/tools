#! /bin/sh

cat /dev/stdin > f_astyle
astyle --style=allman  --indent=spaces=3 --keep-one-line-statements --keep-one-line-blocks --pad-header --indent-cases --indent-switches -q f_astyle 
cat f_astyle
