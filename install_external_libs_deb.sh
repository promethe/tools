#!/bin/sh

################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################

LIBRAIRIES="libgstreamer1.0-dev  libjpeg-dev libmxml-dev libpcre3-dev libdc1394-22-dev libgtk2.0-dev libxt-dev  fftw3-dev libasound2-dev libvte-dev libpython-stdlib libgstreamer-plugins-base0.10-dev libgstreamer0.10-dev gstreamer0.10-tools gstreamer0.10-plugins-base gstreamer0.10-plugins-good nemiver doxygen graphviz python python3 python-pip python-scipy python-matplotlib python-numpy pandoc rlwrap autoconf libtool colorgcc valgrind cpufrequtils libcpufreq0 libgtk-3-dev cmake g++ libbluetooth-dev";

usage()
{
echo "usage: install_external_libs_deb.sh       :install the required libraries. (need the root password)";
echo "       install_external_libs_deb.sh check :check that the required libraries are installed";
};

if [ $# = 0 ];
then
  echo "Installing full librairies for promethe."
  sudo apt-get install $LIBRAIRIES
  exit $?;
else
  if [ "$1" = "check" ];
  then
    dpkg -l $LIBRAIRIES
    exit $?;
  else echo "Unknown parameter '$1'";
  usage;
  exit 1;
  fi
fi

