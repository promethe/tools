include variables.mk

.PHONY:all_checkout all_status all_update default all_tag_release

default:
	@echo  "You need to specify a target (e.g. all_checkout, all_update) ...\n"

update_logfile=$(logdir)/update_$(notdir $*).log
status_logfile=$(logdir)/status_$(notdir $*).log

ifndef login
	login:=$(USER)
endif

ifndef release_name
	release_name:=trunk
endif


#********************* Checkout *******************************
%_checkout:
	svn checkout svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/$(release_name) ../$*

all_without_applications_checkout:$(foreach project, $(projects_without_applications), $(project)_checkout)
	
all_checkout:$(foreach project, $(projects), $(project)_checkout)


#********************* Update *******************************
./Create_shared.sh:./shared_include.txt
	echo update link
	cd ..;./scripts/Create_shared.sh
	touch Create_shared.sh

#up sur les projets voulus
%_update:../% $(logdir)
	@echo "*** Update $(notdir $*) ***" > $(update_logfile)
	@cd ../$* && svn update >> $(update_logfile)
	@echo >> $(update_logfile)
	@cat $(update_logfile)

#up sur tous les projets
all_update:$(foreach project, $(projects),  $(project)_update) $(logdir)
	@echo "Create shared includes"
	@cd .. && ./scripts/Create_shared.sh > $(logdir)/create_shared.log
	

#********************* Status *******************************
%_status:../% $(logdir)
ifdef USE_MELD 
	@cd ../$* && meld .& 
endif
	@echo "*** Status of $(notdir $*) ***" > $(status_logfile)	
	@svn status ../$* --show-updates >> $(status_logfile)
	@echo >> $(status_logfile)
	@cat $(status_logfile)

#status sur tous les projets
all_status:$(foreach project, $(projects), $(project)_status) $(logdir)

#********************* Create tag *******************************


%_tag_release:
ifndef tag_name
	$(error You must specify a 'tag_name')
endif
	svn copy svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/trunk svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/tags/$(tag_name) -m "Tag the current version of $* as tag $(tag_name)"

all_tag_release:$(foreach project, $(projects), $(project)_tag_release)

#********************* Create branch *******************************

%_create_branch:
ifndef branch_name
	$(error You must specify a 'branch_name')
endif
	svn mkdir svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/branches -m "Creation repertoire pour les branches"
	svn copy svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/trunk svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/branches/$(branch_name) -m "Branch the current version of $* as $(branch_name)"

all_create_branch:$(foreach project, $(projects), $(project)_create_branch)

#********************* Switch branch *******************************

#With branch_name=../trunk it should switch back to the main trunk
%_switch_branch:
ifndef branch_name
	$(error You must specify a 'branch_name')
endif
	svn switch svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/branches/$(branch_name) ../$*
	
all_switch_branch:$(foreach project, $(projects), $(project)_switch_branch)
