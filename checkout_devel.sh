#!/usr/bin/env python
# -*- coding: utf-8 -*-
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################

import subprocess
import argparse

parser = argparse.ArgumentParser(description='Installation of promethe framework.')
parser.add_argument('login', type=str, nargs='?', help='login to connect to the subversion server.');
parser.add_argument( '--without-applications', dest='without_applications', action='store_true', help='install everything but the applications directory')

arguments = parser.parse_args()

print('Downloading')
if arguments.login:
	login = "login="+arguments.login
else:
	login = ''

if arguments.without_applications: 
	target = "all_without_applications_checkout"
else:
	target = "all_checkout"
	
subprocess.call('cd scripts && make -j4 --silent '+ target +' '+login + ' && cd .. && ./scripts/install_all_devel.sh', shell=True)

subprocess.call('cd scripts && ./create_prometheconf.sh && cd ..', shell=True)
print('Un mot de passe peut etre requis pour la creation de promethe.conf dans ld.so.conf.d')
subprocess.call('cd scripts && sudo ./copy_prometheconf.sh', shell=True)
