#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################


#Checking the PATH
if ! echo $PATH|grep -q $HOME/bin_leto_prom;
then
   echo -e "\nYou need to add the following lines in your file of preferences ~/.bashrc "
   echo -e "\nexport PATH=\$PATH:\$HOME/bin_leto_prom"
   echo -e "export LC_ALL=C"
   echo -e "\nThen reload your .bashrc ( source ~/.bashrc ) and restart this installation.\n"
   exit 1;
fi

echo
echo "Checking for the required librairies.";
echo

if  ./scripts/install_external_libs_deb.sh check;
then echo "Libraries tested, installation ..."
else
	echo
	echo "Required libraries are not found.";
        echo
	if echo $* | grep -- "--force-install";
	then echo "--force-install, we continue anyway.";
	else echo "Do you want to install them now (you will need the root password) ? [y/n]"
             read answer
 	     if [[ $answer = y ]];
             then 
             ./scripts/install_external_libs_deb.sh;
             else echo "(If you want to install it latter, use: './scripts/install_external_libs_deb.sh' )";
                  echo "Continue anyway ? [y/n] (To avoid this test in the future, use '--force-install')."
                  read answer
                  if [ "$answer" != "y" ];
	          then exit 1
                  fi
             fi
	fi
fi

source scripts/COMPILE_FLAG

mkdir -p $LOGDIR

bash ./scripts/Create_shared.sh > $LOGDIR/Create_shared.log
mkdir -p $HOME/bin_leto_prom
mkdir -p $HOME/bin_leto_prom/glades
mkdir -p $HOME/bin_leto_prom/resources #a terme remplacera glades
mkdir -p $HOME/bin_leto_prom/Libraries/Hardware


ln -sf $(pwd)/scripts/lppreprocesseur/lpreprocess.sh $HOME/bin_leto_prom/
cp -f $(pwd)/scripts/cpu_performance $HOME/bin_leto_prom/
cp -f $(pwd)/scripts/cpu_ondemand $HOME/bin_leto_prom/
ln -sf $(pwd)/scripts/prom_colorgcc $HOME/bin_leto_prom/
ln -sf $(pwd)/scripts/unison-2.40 $HOME/bin_leto_prom/
ln -sf $(pwd)/scripts/promdoc ~/bin_leto_prom/
ln -sf $(pwd)/scripts/promdem ~/bin_leto_prom/
ln -sf $(pwd)/scripts/promnet_create ~/bin_leto_prom/promnet_create
ln -sf $(pwd)/scripts/promnet_add_script ~/bin_leto_prom/promnet_add_script


echo -e "tmpdir:=$TMPDIR" > scripts/local_config.mk
echo -e "logdir:=$LOGDIR" >> scripts/local_config.mk
echo -e "objdir:=$OBJPATH" >> scripts/local_config.mk
echo -e "bindir:=$DIR_BIN_LETO_PROM" >> scripts/local_config.mk
echo -e "rsrcdir:=$DIR_BIN_LETO_PROM/resources" >> scripts/local_config.mk
echo -e "libdir:=$PWD/lib/$SYSTEM" >> scripts/local_config.mk
echo -e "system:=$SYSTEM\n" >> scripts/local_config.mk

echo -e "CFLAGS:=$CFLAGS" >> scripts/local_config.mk
echo -e "FLAGS_DEBUG:=$FLAGS_DEBUG" >> scripts/local_config.mk
echo -e "FLAGS_OPTIM:=$FLAGS_OPTIM" >> scripts/local_config.mk
echo -e "GLIB_INCLUDES:= $GLIBINCLUDES" >> scripts/local_config.mk
echo -e "GTK_INCLUDES:= $GTKINCLUDES" >> scripts/local_config.mk
echo -e "GLIB_LIBS:= $GLIBLIB" >> scripts/local_config.mk
echo -e "GTK_LIBS:= $GTKLIB" >> scripts/local_config.mk
echo -e "BLCLIBPATH:= $PWD/lib/$SYSTEM/blc"  >> scripts/local_config.mk
echo -e "NUMBER_OF_CORES:=$NUMBER_OF_CORES\n" >> scripts/local_config.mk
echo -e "CC:=$CC" >> scripts/local_config.mk
echo -e "AR:=$AR\n" >> scripts/local_config.mk

time scripts/compile_all.sh
result=$?

cp  -f $(pwd)/lib/$SYSTEM/blc/libblc.so $HOME/bin_leto_prom/Libraries

if [[ $result -ne 0 ]]
then
echo "Fail installing promethe "
exit $result
else
ln -f $HOME/.local/lib/libblc.so ~/bin_leto_prom/libblc.so
echo -e "*** Success installing promethe***\n"
echo -e "\nLaunch the demo with themis\n"
~/bin_leto_prom/themis applications/basic_demos.net&
echo -e "\nCompilation of the documentation"
promdoc -c 
promdoc
fi
exit 0



