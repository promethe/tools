#!/bin/bash
################################################################################
# Copyright  ETIS — ENSEA, Université de Cergy-Pontoise, CNRS (1991-2014)
#promethe@ensea.fr
#
# Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier, 
# C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouzène,  
# M. Lagarde, S. Leprêtre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengervé, A. Revel ...
#
# See more details and updates in the file AUTHORS 
#
# This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
# This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software. 
# You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 
# As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license, 
# users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability. 
# In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, 
# that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge. 
# Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured 
# and, more generally, to use and operate it in the same conditions as regards security. 
# The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
################################################################################

source scripts/COMPILE_FLAG

if scripts/install_external_libs_deb.sh check
then echo "Libraries tested, installation ..."
else
	echo "Missing libraries."
	exit 2
fi

mkdir -p $LOGDIR

bash scripts/Create_shared.sh > $LOGDIR/Create_shared.log
mkdir -p $HOME/bin_leto_prom
mkdir -p $HOME/bin_leto_prom/glades
mkdir -p $HOME/bin_leto_prom/resources #a terme remplacera glades
mkdir -p $HOME/bin_leto_prom/Libraries/Hardware

ln -sf $(pwd)/scripts/prom_colorgcc $HOME/bin_leto_prom/

echo -e "tmpdir:=$TMPDIR" > scripts/local_config.mk
echo -e "logdir:=$LOGDIR" >> scripts/local_config.mk
echo -e "objdir:=$OBJPATH" >> scripts/local_config.mk
echo -e "bindir:=$DIR_BIN_LETO_PROM" >> scripts/local_config.mk
echo -e "rsrcdir:=$DIR_BIN_LETO_PROM/resources" scripts/local_config.mk
echo -e "libdir:=$PWD/lib/$SYSTEM" >> scripts/local_config.mk
echo -e "system:=$SYSTEM\n" >> scripts/local_config.mk

echo -e "CFLAGS:=$CFLAGS" >> scripts/local_config.mk
echo -e "FLAGS_DEBUG:=$FLAGS_DEBUG" >> scripts/local_config.mk
echo -e "FLAGS_OPTIM:=$FLAGS_OPTIM" >> scripts/local_config.mk
echo -e "GLIB_INCLUDES:= $GLIBINCLUDES" >> scripts/local_config.mk
echo -e "GTK_INCLUDES:= $GTKINCLUDES" >> scripts/local_config.mk
echo -e "GLIB_LIBS:= $GLIBLIB" >> scripts/local_config.mk
echo -e "GTK_LIBS:= $GTKLIB" >> scripts/local_config.mk
echo -e "NUMBER_OF_CORES:=$NUMBER_OF_CORES" >> scripts/local_config.mk
echo -e "CC:=$CC" >> scripts/local_config.mk
echo -e "AR:=$AR\n" >> scripts/local_config.mk

time scripts/compile_all.sh
result=$?

if [[ $result -ne 0 ]]
then
echo "Fail installing promethe "
exit $result
else
echo -e "*** Success installing promethe***\n"
fi
exit 0



