include variables.mk

.PHONY:all_tag_release tag_release

ifndef login
	login:=$(USER)
endif


%_tag_release:
	svn copy svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/trunk svn+ssh://$(login)@cvs-etis.ensea.fr/svn/SimulateurV1/$*/tags/$(release_name)

all__tag_release:
	$(foreach project, $(projects), $(project)_tag_release) $(logdir)
