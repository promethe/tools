/* gprof-helper.c -- preload library to profile pthread-enabled programs
 *  *
 *   * Authors: Sam Hocevar <sam at zoy dot org>
 *    *          Daniel J�nsson <danieljo at fagotten dot org>
 *     *
 * Copyright  ETIS � ENSEA, Universit� de Cergy-Pontoise, CNRS (1991-2014)
 * promethe@ensea.fr

 * Authors: P. Andry, J.C. Baccon, D. Bailly, A. Blanchard, S. Boucena, A. Chatty, N. Cuperlier, P. Delarboulas, P. Gaussier,
 * C. Giovannangeli, C. Grand, L. Hafemeister, C. Hasson, S.K. Hasnain, S. Hanoune, J. Hirel, A. Jauffret, C. Joulain, A. Karaouz�ne,
 * M. Lagarde, S. Lepr�tre, M. Maillard, B. Miramond, S. Moga, G. Mostafaoui, A. Pitti, K. Prepin, M. Quoy, A. de Rengerv�, A. Revel ...

 * See more details and updates in the file AUTHORS

 * This software is a computer program whose purpose is to simulate neural networks and control robots or simulations.
 * This software is governed by the CeCILL v2.1 license under French law and abiding by the rules of distribution of free software.
 * You can use, modify and/ or redistribute the software under the terms of the CeCILL v2.1 license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * As a counterpart to the access to the source code and  rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights,  and the successive licensors have only limited liability.
 * In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate, and that also therefore means that it is reserved for developers and experienced professionals having in-depth computer knowledge.
 * Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured
 * and, more generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had knowledge of the CeCILL v2.1 license and that you accept its terms.
*/

#define _GNU_SOURCE
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <pthread.h>

static void *wrapper_routine(void *);

/* Original pthread function */
static int (*pthread_create_orig) (pthread_t * __restrict,
                                   __const pthread_attr_t * __restrict,
                                   void *(*)(void *),
                                   void *__restrict) = NULL;

/* Library initialization function */
void wooinit(void) __attribute__ ((constructor));

void wooinit(void)
{
    pthread_create_orig = dlsym(RTLD_NEXT, "pthread_create");
    fprintf(stderr, "pthreads: using profiling hooks for gprof\n");
    if (pthread_create_orig == NULL)
    {
        char *error = dlerror();
        if (error == NULL)
        {
            error = "pthread_create is NULL";
        }
        fprintf(stderr, "%s\n", error);
        exit(EXIT_FAILURE);
    }
}

/* Our data structure passed to the wrapper */
typedef struct wrapper_s
{
    void *(*start_routine) (void *);
    void *arg;

    pthread_mutex_t lock;
    pthread_cond_t wait;

    struct itimerval itimer;

} wrapper_t;

/* The wrapper function in charge for setting the itimer value */
static void *wrapper_routine(void *data)
{
    /* Put user data in thread-local variables */
    void *(*start_routine) (void *) = ((wrapper_t *) data)->start_routine;
    void *arg = ((wrapper_t *) data)->arg;

    /* Set the profile timer value */
    setitimer(ITIMER_PROF, &((wrapper_t *) data)->itimer, NULL);

    /* Tell the calling thread that we don't need its data anymore */
    pthread_mutex_lock(&((wrapper_t *) data)->lock);
    pthread_cond_signal(&((wrapper_t *) data)->wait);
    pthread_mutex_unlock(&((wrapper_t *) data)->lock);

    /* Call the real function */
    return start_routine(arg);
}

/* Our wrapper function for the real pthread_create() */
int
pthread_create(pthread_t * __restrict thread,
               __const pthread_attr_t * __restrict attr,
               void *(*start_routine) (void *), void *__restrict arg)
{
    wrapper_t wrapper_data;
    int i_return;

    /* Initialize the wrapper structure */
    wrapper_data.start_routine = start_routine;
    wrapper_data.arg = arg;
    getitimer(ITIMER_PROF, &wrapper_data.itimer);
    pthread_cond_init(&wrapper_data.wait, NULL);
    pthread_mutex_init(&wrapper_data.lock, NULL);
    pthread_mutex_lock(&wrapper_data.lock);

    /* The real pthread_create call */
    i_return = pthread_create_orig(thread,
                                   attr, &wrapper_routine, &wrapper_data);

    /* If the thread was successfully spawned, wait for the data
     *      * to be released */
    if (i_return == 0)
    {
        pthread_cond_wait(&wrapper_data.wait, &wrapper_data.lock);
    }

    pthread_mutex_unlock(&wrapper_data.lock);
    pthread_mutex_destroy(&wrapper_data.lock);
    pthread_cond_destroy(&wrapper_data.wait);

    return i_return;
}
